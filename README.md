# README #

Algo-Estates. A Property Listing App to showcase ReactJS skills


### This is a property listing App (http://35.165.181.64:8050/) that allows users: ###

* View property listings
* Add Listings (Must be signed in)

The frontEnd is built with ReactJS/Redux Store

The Backend is built with Django and Django rest framework

### The App features: ###

* Sign Up Page
* Sign In Page (JWT used)

### Some of the following React Libraries were used  ###

* jwt-decode : For decoding responses from the Auth server
* reactstrap: For adding bootstrap components
* redux: For managing state
* redux-api-middleware: For making API requests and dispatching corresponding actions
* redux-persist: For persisting the Auth tokens after retrieval


A working demo of the App can be found here http://35.165.181.64:8050/. 
PS: The server is really slow

Here (https://www.dropbox.com/s/i24jj8gliwwgcp3/Algo%20Estates%20App.avi?dl=0) is also a screencast of the App, showcasing some of the features.

