var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')
module.exports = {
	entry: [
		'./src/index.js'
	],
	module: {
		loaders: [
		{
			test: /\.jsx?$/,
			exclude: /node_modules/,
			loader: 'babel-loader'
		},
		{ test: /\.css$/, loader: "style-loader!css-loader" }

		]
	},
	resolve: {
		extensions: ['*', '.js', '.jsx']
	},
	output: {
		path: path.resolve('./assets/bundles/'),
		publicPath: '../static/bundles/',
		filename: '[name]-[hash].js',
	},
	plugins: [
    	new BundleTracker({filename: './webpack-stats.json'}),
  	],
	devServer: {
		contentBase: './dist',
		historyApiFallback: true
	}
};