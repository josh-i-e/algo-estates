import storage from 'redux-persist/es/storage'
import { createStore, applyMiddleware } from 'redux';
import apiMiddleware from '../middleware';
import { createFilter   } from 'redux-persist-transform-filter';
import { persistReducer, persistStore } from 'redux-persist'
import { createLogger } from 'redux-logger';
import { browserHistory } from 'react-router';

import { routerMiddleware } from 'react-router-redux';
import rootReducer from '../reducers/index';

const logger = createLogger();

export default (history) => {
  	const persistedFilter = createFilter(
		'auth', ['access', 'refresh']);
  	const reducer = persistReducer(
		{
	  		key: 'polls',
	  		storage: storage,
	  		whitelist: ['auth'],
	  		transforms: [persistedFilter]
		},
		rootReducer)
  	const store = createStore(
		reducer, {},
		applyMiddleware(
	  	apiMiddleware, 
	  	logger,
	  	routerMiddleware(history))
  	)
  	return store
}