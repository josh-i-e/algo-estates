import jwtDecode from 'jwt-decode'
import * as listing from '../actions/listing'
const initialState = {}

export default (state=initialState, action) => {
	switch(action.type) {
		case listing.FETCH_LISTING_SUCCESS:
			return {
				...state,
				...action.payload
			}

		case listing.FETCH_LISTING_FAILURE:
			return {
				...state,
				errors: 
					action.payload.response || 
								{'non_field_errors': action.payload.statusText},
			}
		default:
			return state
		}
}


export function accessListings(state) {
    if (state.fetchListing) {
        return  state.fetchListing
    }
}