import jwtDecode from 'jwt-decode'
import * as addListing from '../actions/addListing'
const initialState = {
	errors: {},
	message: null
}
export default (state=initialState, action) => {
	switch(action.type) {
		case addListing.ADD_SUCCESS:
			return {
				...state,
				errors: {},
				message: "Posting successful. You will be redirected in 3 seconds."			
		}

		case addListing.ADD_FAILURE:
			return {
				...state,
				errors: 
					action.payload.response || 
						{'non_field_errors': action.payload.statusText}
			}
		default:
			return state
		}
}

export function errors(state) {
	return state.addListing.errors
}
export function message(state) {
	return state.addListing.message
}