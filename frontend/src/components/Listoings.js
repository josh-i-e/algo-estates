import React from 'react';
import { connect } from 'react-redux';

class Listings extends React.Component {

	constructor(props) {
    	super(props);
    	//this.state = {};
  	}

  	componentWillMount() {
    	// do things
  	}

  	componentDidUpdate() {
    	// do things
  	}

	render() {
		//console.log(this.props);

		//const { listings = [] } = this.props;
		const listings  = this.props.listings;
 		console.log(listings.length);
 		
 		/*
 		<button onClick={() => this.setState({ [key]: !this.state[key] })} type="button">
            { this.state[key] ? 'Dislike' : 'Like' }
        </button>
        */

		return (
	  		<div>
				{
					listings.map((listing, key) => {
						return <div className='listing' key={listing.id}>
							<p>Name: {listing.name}</p>
							<p>Description: {listing.description}</p>
							<p>Type: {listing.type}</p>
							<p>Amount: {listing.amount}</p>
							<button type="button">
                  				
                			</button>
						</div>;
		  			})
				}
	  		</div>
		);
		
  	}

}

function mapStateToProps(state) {
	const listings = state.listing;
  	return {
    	listings
  	}
}

export default connect(mapStateToProps)(Listings);

