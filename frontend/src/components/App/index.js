import React from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

function App({ children }) {
 	return <div>
		<Navbar color="faded" light expand="md">
			<NavbarBrand href="/">AlgoEstates</NavbarBrand>
		  	<Nav className="ml-auto" navbar>
			 	<NavItem>
					<NavLink href="/">Home</NavLink>
			  	</NavItem>
			  	<NavItem>
					<NavLink href="/add/">Add Listing</NavLink>
			  	</NavItem>
			  	<NavItem>
					<NavLink href="/login">Login</NavLink>
			  	</NavItem>
			  	<NavItem>
					<NavLink href="/signup">SignUp</NavLink>
			  	</NavItem>
			</Nav>
	   	</Navbar>
	   	{children}
	</div>;
}

export default App;