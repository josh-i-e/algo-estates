import React from 'react';
import { Container, Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, CardLink, Badge, Button } from 'reactstrap';
import TextInput from '../TextInput';

class Listings extends React.Component {
	
	constructor(props) {
    	super(props);
    	
    	this.state = {
			initialListings : {},
			listings : {},
			initialResult : [],
			result : [],
			fields: []
		};
  	}
	
	componentDidMount() {
		this.props.fetchSetListings();
	}

	componentWillReceiveProps(props) {
		this.setState({
			listings : props.listings,
			initialListings : props.listings
		})
		const listings = props.listings;
		const result = this.objectToList(listings);
		this.setState({
			initialResult: result,
			result: result
		});
		let fields = []
		if (result.length > 0 ){
			fields = Object.keys(result[0])
			this.setState({fields: fields});
		}
	}

	objectToList(obj) {
		let result = []		
		for (let x in obj) {			
			if( obj.hasOwnProperty(x) ) {
				result.push(obj[x]);
			} 
		} 
		return result;
	}

	filterList = (event) => {
		event.preventDefault()
		let initialList = this.state.initialResult;
		let key = this.state.propertySelect 
		let updatedList = initialList.filter(function(item){
			return item[key].toString().toLowerCase().search(
				event.target.value.toString().toLowerCase()) !== -1;
		});
		this.setState({result: updatedList});
	}

	propertySelect = (event) => {
		event.preventDefault()
		this.setState({propertySelect: event.target.value});
	}

	btnClick = (id) => {
		event.preventDefault()
		this.props.history.push('/listing/id/'+id+'/')
	}


	render() {
		if ('result'){
		return ([
			<div>
				<Container>
					<Form>						
						<Row>
							<Col xs="2">
								<FormGroup>
									<Input type="select" className="form-control" onChange={this.propertySelect}>
										<option></option>
									{
										this.state.fields.map((field) => {
											return <option>{field}</option>
										})	
									}												
									</Input>
		  						</FormGroup>		  						
							</Col>							
							<Col xs="6">
								<FormGroup>
									<TextInput type="text" className="form-control" placeholder="Search" onChange={this.filterList}/>
								</FormGroup>
							</Col>
						</Row>						
					</Form>
					

					<Row>
					{
					this.state.result.map((listing, key) => {
						return <Col md="3" sm="12"><div className='listing' key={listing.id}>
							<Card>
								<CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" />
								<CardBody>
									<CardTitle>{listing.name}</CardTitle>
									<CardSubtitle>{listing.location}</CardSubtitle>
									<CardText>{listing.description}</CardText>
									<Button onClick={e => this.btnClick(listing.id)}>View</Button>
									<CardText>
            							<small className="text-muted">Posted by: {listing.user.toUpperCase()}</small>
          							</CardText>
								</CardBody>
							</Card>							
						</div>
						</Col>;
					})
				}
					</Row>
				</Container>
			</div>
		]);
		}
		else{
			return(<div>Loading...</div>)
		}		
	}
}

export default Listings;



