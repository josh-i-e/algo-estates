import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Listings from './presenter';
import {fetchSetListings} from  '../../actions/listing';
import {accessListings} from '../../reducers/fetchListing'

const mapStateToProps = (state) => ({
	listings : accessListings(state)
})

const mapDispatchToProps = (dispatch) => ({
	fetchSetListings: () => {	
		dispatch(fetchSetListings())
  	}
})

export default connect(mapStateToProps, mapDispatchToProps)(Listings);