import React from 'react';
import { Alert, Button, Jumbotron,  Form } from 'reactstrap';
import TextInput from '../TextInput'


export default class SignUpForm extends React.Component {
	state = {
			username: '',
			password: '',
			bTN: 'Sign Up',
		}
	componentWillReceiveProps(props) {
		this.setState({
			bTN: 'Sign Up',
		})
		console.log('received props')
		console.log(props)
		if(props.isCreated) {
			console.log('authed')
			props.history.push('/login')
		}
	}
	handleInputChange = (event) => {
		const target = event.target,
		value = target.type === 
			'checkbox' ? target.checked : target.value,
		name = target.name
		this.setState({
			[name]: value
		});
	}
	onSubmit = (event) => {
		event.preventDefault()
		this.setState({
			bTN: "Posting"
		})
		this.props.onSubmit(this.state.username, this.state.password)		
	}
	render() {
		const errors = this.props.errors || {}
		return (
			<Jumbotron className="container">
				<Form onSubmit={this.onSubmit}>
					<h1>Sign Up</h1>
						{
							errors.non_field_errors?
							<Alert color="danger">
							{errors.non_field_errors}
							</Alert>:""
						}
					<TextInput name="username" label="Username" 
						 error={errors.username}
						 onChange={this.handleInputChange}
					/>
					<TextInput name="password" label="Password" 
						error={errors.password} type="password"  
						onChange={this.handleInputChange}/>
								
					<Button type="submit" color="primary" size="lg">
						{this.state.bTN}
					</Button>
					</Form>
			</Jumbotron>
			)
		}
}