import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import SignUpForm from './presenter'

import {signup} from  '../../actions/auth'
import {authErrors} from '../../reducers'
import {isCreated} from '../../reducers/auth'
/*
const Login = (props) => {
	if(props.isAuthenticated) {
		return (
			<Redirect to='/' />
		)
		return (....
			<div className="login-page">
				<LoginForm {...props}/>
			</div>
		)
}
*/
const mapStateToProps = (state) => ({
	errors: authErrors(state),
	isCreated: isCreated(state)
})
const mapDispatchToProps = (dispatch) => ({
	onSubmit: (username, password) => {
		dispatch(signup(username, password))
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm);