import React from 'react';
import { Container, Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, CardLink, Badge, Button } from 'reactstrap';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { ListGroup, ListGroupItem } from 'reactstrap';
import TextInput from '../TextInput';

class Listing extends React.Component {
	
	constructor(props) {
    	super(props);
    
    	this.state = {
			initialListings : {},
			listings : {},
			initialResult : [],
			result : [],
			fields: [],
			modal: true,
			data: {}
		};
  }
	
	componentDidMount() {
		this.props.fetchSetListings();
		console.log(this.props.match.params.id)
	}

	componentWillReceiveProps(props) {
		this.setState({
			listings : props.listings,
			initialListings : props.listings
		})
		const listings = props.listings;
		const initResult = this.objectToList(listings);
		const result = initResult.reverse();
		this.setState({
			initialResult: result,
			result: result
		});
		let fields = []
		if (result.length > 0 ){
			fields = Object.keys(result[0])
			this.setState({fields: fields});
		}
		this.setState({data: result[props.match.params.id -1]});
		this.setState({modal: true});
	}

	objectToList(obj) {
		let result = []		
		for (let x in obj) {			
			if( obj.hasOwnProperty(x) ) {
				result.push(obj[x]);
			} 
		} 
		return result;
	}

	filterList = (event) => {
		event.preventDefault()
		let initialList = this.state.initialResult;
		let key = this.state.propertySelect || 'beds'
		console.log(this.state.propertySelect)
		let updatedList = initialList.filter(function(item){
			return item[key].toString().toLowerCase().search(
				event.target.value.toString().toLowerCase()) !== -1;
		});
		this.setState({result: updatedList});
	}

	propertySelect = (event) => {
		event.preventDefault()
		this.setState({propertySelect: event.target.value});
	}

	btnClick = (id) => {
		event.preventDefault()
		this.props.history.push('/listing/id/'+id+'/')
	}

	toggle = (event) => {
		this.setState({
    		modal: !this.state.modal
    	})
	}


	render() {		
		
		return ([
			<div>
				<Container>
					<Form>						
						<Row>
							<Col xs="2">
								<FormGroup>
									<Input type="select" className="form-control" onChange={this.propertySelect}>
									{
										this.state.fields.map((field) => {
											return <option>{field}</option>
										})	
									}												
									</Input>
		  						</FormGroup>		  						
							</Col>							
							<Col xs="6">
								<FormGroup>
									<TextInput type="text" className="form-control" placeholder="Search" onChange={this.filterList}/>
								</FormGroup>
							</Col>
						</Row>						
					</Form>
					<Row>
					{
					this.state.result.map((listing, key) => {
						return <Col xs="3">
							<div className='listing' key={listing.id}>
							<Card>
								<CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt={listing.description} />
								<CardBody>
									<CardTitle>{listing.name}</CardTitle>
									<CardSubtitle>Card subtitle</CardSubtitle>
									<CardText>{listing.description}</CardText>
									<Button onClick={e => this.btnClick(listing.id)}>View</Button>
								</CardBody>
							</Card>
							</div>
						</Col>;
					})
					}
					</Row>
				</Container>
			</div>,
			<div>
        		<Modal isOpen={this.state.modal} toggle={this.toggle}>
          			<ModalHeader >{this.state.data.name}</ModalHeader>
          			<ModalBody>
            			<ModalBody>
          				<ListGroup>
        					<ListGroupItem>Description: {this.state.data.description}</ListGroupItem>
        					<ListGroupItem>Location: {this.state.data.location}</ListGroupItem>
        					<ListGroupItem>Type: {this.state.data.propertyType}</ListGroupItem>
        					<ListGroupItem>Beds: {this.state.data.beds}</ListGroupItem>
        					<ListGroupItem>Bath: {this.state.data.bath}</ListGroupItem>
        					<ListGroupItem>Toilet: {this.state.data.toilet}</ListGroupItem>
        					<ListGroupItem>Amount: ₦ {this.state.data.amount}</ListGroupItem>
        					<ListGroupItem>Posted By: {this.state.data.user}</ListGroupItem>
        					<ListGroupItem>Posted On: {this.state.data.date}</ListGroupItem>
      					</ListGroup>
          			</ModalBody>
          			</ModalBody>
     	     		<ModalFooter>
        	    		<Button color="danger" onClick={this.toggle}>Close</Button>
        			</ModalFooter>
        		</Modal>
      		</div>


		]);
				
	}

}

export default Listing;



