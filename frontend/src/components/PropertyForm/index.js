import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import PropertyForm from './presenter'

import {add} from  '../../actions/addListing'
import {isAuthenticated} from '../../reducers'
import {errors, message} from '../../reducers/addListing'
/*
const Login = (props) => {
  if(props.isAuthenticated) {
    return (
      <Redirect to='/' />
    )
    return (....
      <div className="login-page">
        <LoginForm {...props}/>
      </div>
    )
}
*/
const mapStateToProps = (state) => ({
    errors: errors(state),
    isAuthenticated: isAuthenticated(state),
    message: message(state)
})
const mapDispatchToProps = (dispatch) => ({
  onSubmit: (data) => {
    dispatch(add(data))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(PropertyForm);