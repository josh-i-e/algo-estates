import React from 'react';
import { Alert, Button} from 'reactstrap';
import TextInput from '../TextInput'
import { Container, Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class PropertyForm extends React.Component {

	constructor(props) {
    	super(props);
    	
    	if(!props.isAuthenticated) {
			props.history.push("/login")
		}

    	this.state = {
			data: {},
			bTN: 'Post',
			message: null
		};
  	}

	handleInputChange = (event) => {
		const target = event.target,
		value = target.type === 
			'checkbox' ? target.checked : target.value,
		name = target.name
		this.setState({
			data:{...this.state.data, [name]: value}
		})
	}
	componentWillReceiveProps(props) {
		this.setState({
			bTN: 'Post',
			message: props.message
		})
	}
	componentWillUnmount() {
    	clearInterval(this.interval);
  	}
	redirect = () => {
		this.interval = setInterval(() => {this.props.history.push("/")}, 3000)
		return(
      		<Modal isOpen = {true}>
          		<ModalBody>
            		{this.state.message}
          		</ModalBody>
        	</Modal>
        )		
	}
	onSubmit = (event) => {
		event.preventDefault()
		this.setState({
			bTN: "Posting"
		})
		console.log(this.state.data)
		this.props.onSubmit(this.state.data)
	}
	render() {
		const errors = this.props.errors || {}

		return (			
			<Container>
				<Row>
					<Col xs="3"></Col>
					<Col xs="6">
						<Form onSubmit={this.onSubmit}>						
							<FormGroup>
								<h1>Add Property</h1>
								{
								errors.non_field_errors?
								<Alert color="danger">
								{errors.non_field_errors}
								</Alert>:""
								}
								{errors.code?
								<Alert color="danger">
								{errors.code}
								</Alert>:""
								}
								<TextInput name="name" label="Name" 
									 error={errors.name}
									 onChange={this.handleInputChange}
								/>
								<TextInput name="location" label="Location" 
									 error={errors.location}
									 onChange={this.handleInputChange}
								/>
								<TextInput type="textarea" name="description" label="Description" 
								 error={errors.description} 
								 onChange={this.handleInputChange}/>

					 			<FormGroup>
					 			<Label>Property Type</Label>
					 			<Input type="select" name="propertyType" className="form-control" onChange={this.handleInputChange}>
									<option></option>
									<option value="Duplex">Duplex</option>
									<option value="Flat">Flat</option>
									<option value="BQ">BQ</option>
								</Input>
								</FormGroup>

								<TextInput name="amount" label="Amount" 
					 			error={errors.amount} 
					 			onChange={this.handleInputChange}/>

					 			<TextInput name="beds" label="Beds" 
					 			error={errors.beds} 
					 			onChange={this.handleInputChange}/>					 			

					 			<TextInput name="bath" label="Bath" 
					 			error={errors.bath} 
					 			onChange={this.handleInputChange}/>

					 			<TextInput name="toilet" label="Toilet" 
					 			error={errors.toilet} 
					 			onChange={this.handleInputChange}/>
								
								<Button type="submit" color="primary" size="lg" className="pull-right">
									{this.state.bTN}
								</Button>
							</FormGroup>
						</Form>
					</Col>
					<Col xs="3"></Col>
				</Row>
				{
					this.state.message?
					this.redirect():""

				}				
			</Container>
			
		)
	}
}