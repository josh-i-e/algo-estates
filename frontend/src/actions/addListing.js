import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers'

export const ADD_REQUEST = '@@add/ADD_REQUEST';
export const ADD_SUCCESS = '@@add/ADD_SUCCESS';
export const ADD_FAILURE = '@@add/ADD_FAILURE';

export const add = (data) => ({
  [RSAA]: {
	  endpoint: 'http://35.165.181.64:8050/api/listings/add/',
	  method: 'POST',
	  body: JSON.stringify(data),
	  headers: withAuth({ 'Content-Type': 'application/json' }),
	  types: [
		ADD_REQUEST, ADD_SUCCESS, ADD_FAILURE
	  ]
  }
})