import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers'

export const FETCH_LISTING_REQUEST = '@@fetchlisting/FETCH_LISTING_REQUEST';
export const FETCH_LISTING_SUCCESS = '@@fetchlisting/FETCH_LISTING_SUCCESS';
export const FETCH_LISTING_FAILURE = '@@fetchlisting/FETCH_LISTING_FAILURE';

export const fetchSetListings = () => ({
  	[RSAA]: {
		endpoint: 'http://35.165.181.64:8050/api/listings/fetch/',
	 	method: 'GET',
	 	types: [
			FETCH_LISTING_REQUEST, FETCH_LISTING_SUCCESS, FETCH_LISTING_FAILURE
	 	]
  	}
})