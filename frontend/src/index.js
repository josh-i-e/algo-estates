import React from 'react';
import ReactDOM from 'react-dom';
//import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { BrowserRouter, Switch, Route, } from 'react-router-dom'
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';
import configureStore from './stores/configureStore';
import * as actions from './actions';
import App from './components/App';
import Listings from './components/Listings'
import Listing from './components/Listing'
import LoginForm from './components/LoginForm'
import SignUpForm from './components/SignUpForm'
import PropertyForm from './components/PropertyForm'
import 'bootstrap/dist/css/bootstrap.css';
import { persistReducer, persistStore } from 'redux-persist'


const store = configureStore();
//store.dispatch(actions.fetchSetListings(listings));


const history = syncHistoryWithStore(createBrowserHistory(), store);

persistStore(store, {}, () => {
	ReactDOM.render(
		<Provider store={store}>
			<BrowserRouter>
				<App>        
					<Switch>
						<Route exact path="/" component={Listings} /> 
						<Route path="/login/" component={LoginForm} />     		
						<Route path="/add/" component={PropertyForm} />  
						<Route path="/signup/" component={SignUpForm} />  
						<Route path="/listing/id/:id/" component={Listing} />  
					</Switch>   		
				</App>
			</BrowserRouter>
		</Provider>, 
		document.getElementById('app')
		);

})

