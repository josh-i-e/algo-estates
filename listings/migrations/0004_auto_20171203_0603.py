# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-03 05:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0003_listings_location'),
    ]

    operations = [
        migrations.AlterField(
            model_name='listings',
            name='propertyType',
            field=models.CharField(choices=[('Duplex', 'Duplex'), ('Flat', 'Flat'), ('BQ', 'BQ')], max_length=20),
        ),
    ]
