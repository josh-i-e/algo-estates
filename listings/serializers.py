from rest_framework import serializers
from .models import Listings
from django.contrib.auth.models import User

class addListingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Listings
        exclude = ('pub_date', 'user',)

class fetchListingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Listings
        exclude = ('pub_date', 'user',)

class signUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')

