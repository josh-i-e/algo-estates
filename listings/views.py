# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from listings.serializers import addListingSerializer, fetchListingSerializer, signUpSerializer
from rest_framework import views, status
from rest_framework.response import Response
from django.core import serializers
from .models import Listings
import json
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny
from django.contrib.auth.models import User
from django.shortcuts import render

# Create your views here.


def index(request):
    return render(request, 'index.html')


class addListingView(views.APIView):
	def post(self, request, *args, **kwargs):
		data = request.data.copy()
		serializer = addListingSerializer(data=data)
		serializer.is_valid(raise_exception=True)
		try:
			serializer.save(user=request.user)
			response = {
				'code': '00',
				'status': 'success'
			} 
			return Response(
				response, status=status.HTTP_201_CREATED)
		except:
			response = {
				'code': '01',
				'status': 'failed'
			}     
			return Response(
				response, status=status.HTTP_400_BAD_REQUEST)


class fetchListingView(views.APIView):
	permission_classes = (IsAuthenticatedOrReadOnly,)
	def get(self, request, *args, **kwargs):
		listings = Listings.objects.all().order_by('-pub_date')
		data = [
			{
				'id': item.id,
				'user': item.user.username, 
				'name': item.name,
				'location': item.location,
				'description': item.description,
				'amount': item.amount,
				'propertyType': item.propertyType,
				'beds': item.beds,
				'bath': item.bath,
				'toilet': item.toilet,
				'date': item.pub_date
			} for item in listings
		]
		
		return Response(
			data, status=status.HTTP_201_CREATED)
		"""
		serializer = fetchListingSerializer(listings, many=True)
		return Response(serializer.data)
		"""

class signupView(views.APIView):
	permission_classes = (AllowAny,)
	def post(self, request, *args, **kwargs):
		data = request.data.copy()
		serializer = signUpSerializer(data=data)
		serializer.is_valid(raise_exception=True)
		try:
			user = User.objects.create_user(**serializer.data)
			response = {
				'code': '00',
				'status': 'success'
			} 
			return Response(
				response, status=status.HTTP_201_CREATED)
		except:
			response = {
				'code': '01',
				'status': 'failed',
				'errors': serializer.errors
			}     
			return Response(
				response, status=status.HTTP_400_BAD_REQUEST)