# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Listings(models.Model):

	PROPERTY_TYPE_CHOICES = (
		('Duplex', 'Duplex'),
		('Flat', 'Flat'),
		('BQ', 'BQ'),
	)

	user = models.ForeignKey(User, on_delete=models.CASCADE,)
	name = models.CharField(max_length=200)
	description = models.TextField()
	location = models.CharField(max_length=200)
	amount = models.PositiveIntegerField()
	propertyType = models.CharField(
		max_length=20,
		choices=PROPERTY_TYPE_CHOICES,
	)
	beds = models.PositiveSmallIntegerField()
	bath = models.PositiveSmallIntegerField()
	toilet = models.PositiveSmallIntegerField()
	pub_date = models.DateTimeField(auto_now_add=True)


