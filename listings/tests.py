# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.contrib.auth.models import User
from django.db import IntegrityError  

# Create your tests here.

class UserModelTests(TestCase):

    def test_duplicate_username(self):
        """
        duplicate_username() raises IntegrityError for username that already exists.
        """
        User.objects.create_user('hala')
        self.assertRaises(IntegrityError, User.objects.create_user, 'hala')